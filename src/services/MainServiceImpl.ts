/* eslint-disable @typescript-eslint/no-explicit-any */
import 'reflect-metadata';
import { inject, injectable } from 'inversify';
import { TYPES } from '../utils/Constants';
import { Logger } from '../utils/logger/Logger';
import { MainPresenter } from '../presenters/MainPresenter';
import { MainService } from './MainService';
import { RequestApiGWModel } from '../models/RequestApiGWModel';
import { DateAdapter } from '../adapters/date/DateAdapter';
import { DNAAdapter } from '../adapters/dna/DNAAdapter';
import { DatabaseAdapter } from '../adapters/database/DatabaseAdapter';

@injectable()
export class MainServiceImpl implements MainService {
    constructor(
        @inject(TYPES.DNAAdapter)
        private dnaProvider: DNAAdapter,
        @inject(TYPES.DatabaseAdapter)
        private databaseProvider: DatabaseAdapter,
        @inject(TYPES.DateAdapter)
        private dateProvider: DateAdapter,
        @inject(TYPES.MainPresenter)
        private presenter: MainPresenter,
        @inject(TYPES.Logger) private LOGGER: Logger
    ) {}
    async processData(payload: RequestApiGWModel): Promise<any> {
        this.LOGGER.debug(payload);
        const date: string = this.dateProvider.getCurrentDateUTC();
        try {
            const isMutant: boolean = this.dnaProvider.isMutant(payload);
            await this.databaseProvider.save(date, isMutant);
            if (isMutant) {
                return this.presenter.generateMutantResponse({
                    date,
                });
            } else {
                return this.presenter.generateHumanResponse({
                    date,
                });
            }
        } catch (error) {
            this.LOGGER.error({ error });
            return this.presenter.generateInternalErrorResponse(
                'Internal error, dont get urlsigned'
            );
        }
    }
}
