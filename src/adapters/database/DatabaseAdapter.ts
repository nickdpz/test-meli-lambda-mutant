export interface DatabaseAdapter {
    save(date: string, isMutant: boolean): Promise<void>;
}
