import { RequestApiGWModel } from '../../models/RequestApiGWModel';

export interface DNAAdapter {
    isMutant(dna: RequestApiGWModel): boolean;
}
