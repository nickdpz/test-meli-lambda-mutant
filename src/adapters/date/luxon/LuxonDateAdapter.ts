import { DateAdapter } from '../DateAdapter';
import { DateTime } from 'luxon';
import { injectable } from 'inversify';

@injectable()
export class LuxonDateAdapter implements DateAdapter {
    getCurrentDateUTC(): string {
        return DateTime.now().toUTC().toString();
    }
}
