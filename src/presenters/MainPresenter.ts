/* eslint-disable @typescript-eslint/no-explicit-any */
import { ResponseApiGWModel } from '../models/ResponseApiGWModel';
export interface MainPresenter {
    generateMutantResponse(body?: any): ResponseApiGWModel;
    generateHumanResponse(body?: any): ResponseApiGWModel;
    generateInternalErrorResponse(message: string): ResponseApiGWModel;
    generateErrorRequestResponse(message: string): ResponseApiGWModel;
}
