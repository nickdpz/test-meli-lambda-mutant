import 'reflect-metadata';
import { APIGatewayEvent } from 'aws-lambda';
import { ApiGWController } from '../../../src/controllers/apigw/ApiGWController';

describe('Controller Test Suite', () => {
    const serviceSpy = jasmine.createSpyObj('MainService', ['processData']);
    const processDataServiceMock = serviceSpy.processData as jasmine.Spy;
    let controller: ApiGWController;

    const EVENT: APIGatewayEvent = {
        body: JSON.stringify({
            dna: ['ATGCGA', 'CAGTGC', 'TTATGC', 'AGAAGG', 'CCCCTA', 'TCACTG'],
        }),
        headers: {},
        httpMethod: 'POST',
        isBase64Encoded: false,
        multiValueHeaders: {},
        path: '',
        pathParameters: {},
        multiValueQueryStringParameters: {},
        stageVariables: {},
        queryStringParameters: {},
        requestContext: undefined,
        resource: '',
    };

    const RESPONSE_SUCCESS = 'OK';

    beforeEach(() => {
        controller = new ApiGWController(serviceSpy);
    });

    it('should create a controller', () => {
        expect(controller).toBeDefined();
    });

    it('should get controller response OK', async () => {
        processDataServiceMock.and.resolveTo(RESPONSE_SUCCESS);
        await expectAsync(controller.handleEvent(EVENT)).toBeResolved();
    });

    it('should get controller error', async () => {
        processDataServiceMock.and.rejectWith('Error');
        await expectAsync(controller.handleEvent(EVENT)).toBeRejectedWith('Error');
    });
});
