/* eslint-disable @typescript-eslint/no-explicit-any */
import * as AWSMock from 'aws-sdk-mock';
import * as AWS from 'aws-sdk';
import { DynamoDatabaseAdapter } from '../../../src/adapters/database/dynamo/DynamoDatabaseAdapter';
import { DatabaseAdapter } from '../../../src/adapters/database/DatabaseAdapter';
import { WinstonLogger } from '../../../src/utils/logger/winston/WinstonLogger';

describe('DynamoDatabaseAdapter Test Suite', () => {
    it('adapater save successfully', async () => {
        AWSMock.setSDKInstance(AWS);
        AWSMock.mock(
            'DynamoDB.DocumentClient',
            'put',
            (parameters: any, callback: (str: string, result: any) => void) => {
                console.log('Intro mock dynamo success');
                callback(null, {});
            }
        );
        const adapter: DatabaseAdapter = new DynamoDatabaseAdapter(new WinstonLogger());
        await expectAsync(adapter.save('2021-29-07', true)).toBeResolved();
        AWSMock.restore('DynamoDB.DocumentClient');
    });
    it('adapater throw error', async () => {
        AWSMock.setSDKInstance(AWS);
        AWSMock.mock(
            'DynamoDB.DocumentClient',
            'put',
            (parameters: any, callback: (str: string) => void) => {
                console.log('Intro mock dynamo error');
                callback('Error');
            }
        );
        const adapter: DatabaseAdapter = new DynamoDatabaseAdapter(new WinstonLogger());
        await expectAsync(adapter.save('2021-29-07', true)).toBeRejected();
        AWSMock.restore('DynamoDB.DocumentClient');
    });
});
